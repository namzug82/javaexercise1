package java.exercises;

/**
 * Hello world!
 *
 */
public class Greeting implements IGreeting
{
    public String salute()
    {
        return "Hello World!";
    }
}
