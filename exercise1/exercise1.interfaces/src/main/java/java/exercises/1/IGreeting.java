package java.exercises;

public interface IGreeting 
{
    public String salute();
}
