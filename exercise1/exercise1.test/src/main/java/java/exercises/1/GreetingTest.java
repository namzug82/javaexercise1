package java.exercises;

import org.junit.Test;
import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.containsString;
import java.exercises.IGreeting;
import java.exercises.Greeting;

public class GreetingTest 
{
	@Test
    public void testSalute()
    {
		IGreeting greeting = new Greeting();
		assertThat(greeting.salute(), containsString("Hello World!"));
    }
}
